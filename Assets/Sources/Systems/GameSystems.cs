public sealed class GameSystems : Feature
{
    public GameSystems(Contexts contexts)
    {
        Add(new ControlsSystem(contexts));
        Add(new CheckSpawnTargetSystem(contexts));
        Add(new SetInsideCupSystem(contexts));

        Add(new ProduceUidSystem(contexts));
        Add(new TransformSystem(contexts));
        Add(new RotateOnStayStillSystem(contexts));


        Add(new TrashEventSystem(contexts));
    }
}