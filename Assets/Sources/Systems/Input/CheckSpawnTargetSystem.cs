using System.Collections.Generic;
using Entitas;
using Scripts.Game.Unit;
using UnityEngine;

public class CheckSpawnTargetSystem : ReactiveSystem<InputEntity>, IInitializeSystem
{
    private InputContext _context;
    private InputEntity _mouseEntity;
    private InputEntity _spawnFactoryIdEntity;
    public CheckSpawnTargetSystem(Contexts contexts) : base(contexts.input)
    {
        _context = contexts.input;
        _mouseEntity = contexts.input.clickEntity;
    }
    public void Initialize()
    {
        _spawnFactoryIdEntity = _context.SetSpawnFactoryId("frog");
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.AllOf(InputMatcher.TargetUnit, InputMatcher.Click));
    }


    protected override bool Filter(InputEntity entity)
    {
        return entity.hasTargetUnit && entity.targetUnit.value != null;
    }
    protected override void Execute(List<InputEntity> entities)
    {
        foreach (var entity in entities)
        {
            var tapPosition = (Vector2)Camera.main.ScreenToWorldPoint(entity.click.value);
            entity.targetUnit.value.OnTap(tapPosition);
            if (entity.targetUnit.value.Spawner)
            {
                var factroyEntity = Contexts.sharedInstance.productFactory.GetEntityWithFactoryId(_spawnFactoryIdEntity.spawnFactoryId.value);
                var productUnit = factroyEntity.factoryUnit.value.Spawn();
                productUnit.transform.position = tapPosition;
            }
            else
            {

            }
        }
    }

}