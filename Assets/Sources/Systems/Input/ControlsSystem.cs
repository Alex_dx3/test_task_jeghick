using Entitas;
using Scripts.Game.Unit;
using UnityEngine;

public class ControlsSystem : IInitializeSystem, IExecuteSystem
{
    readonly InputContext _context;
    private InputEntity _mouseEntity;
    public ControlsSystem(Contexts contexts)
    {
        _context = contexts.input;
    }

    public void Initialize()
    {
        _context.SetClick(Input.mousePosition);
        _mouseEntity = _context.clickEntity;
    }

    public void Execute()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Click");
            _mouseEntity.ReplaceClick(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                Debug.Log("Target Position: " + hit.collider.gameObject.transform.position);

                _mouseEntity.ReplaceTargetUnit(hit.collider.transform.GetComponent<BaseUnit>());
            }
            else
            {
                if (_mouseEntity.hasTargetUnit) _mouseEntity.RemoveTargetUnit();
            }
        }
    }
}