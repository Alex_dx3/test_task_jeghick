using Entitas;
using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

public class SetInsideCupSystem : ReactiveSystem<InputEntity>
{
    public SetInsideCupSystem(Contexts contexts) : base(contexts.input)
    {
    }

    protected override void Execute(System.Collections.Generic.List<InputEntity> entities)
    {
        foreach (var entity in entities)
        {
            var mobEntity = Contexts.sharedInstance.game.GetEntityWithFrogUid(entity.targetUid.value.uid);
            if (mobEntity != null)
            {
                mobEntity.isInsideCup =
                    entity.isUnitEnterCup ?
                    true : entity.isUnitExitCup ?
                    false : mobEntity.isInsideCup;
            }
        }
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.hasTargetUid;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.AnyOf(InputMatcher.UnitEnterCup, InputMatcher.UnitExitCup));
    }
}