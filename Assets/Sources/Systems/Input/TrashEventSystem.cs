using System.Collections.Generic;
using Entitas;
using Scripts.Game.Unit;
using UnityEngine;

public class TrashEventSystem : ReactiveSystem<InputEntity>
{
    public TrashEventSystem(Contexts contexts) : base(contexts.input)
    {
    }

    protected override void Execute(List<InputEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.Destroy();
        }
    }

    protected override bool Filter(InputEntity entity)
    {
        return true;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.EventFlag);
    }
}