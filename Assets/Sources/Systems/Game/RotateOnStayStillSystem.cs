using Entitas;
using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

public class RotateOnStayStillSystem : ReactiveSystem<GameEntity>
{
    public RotateOnStayStillSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override void Execute(System.Collections.Generic.List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.mobUnit.value.OnStayStill();
        }
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.speed.value == 0 && entity.direction.value != 0 && !entity.isInsideCup;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Speed);
    }
}