using Entitas;
using Scripts.Game.Unit;
using UnityEngine;

public class TransformSystem : IExecuteSystem
{
    readonly GameContext _context;
    readonly IGroup<GameEntity> _mobs;
    public TransformSystem(Contexts contexts)
    {
        _mobs = contexts.game.GetGroup(GameMatcher.MobUnit);
        _context = contexts.game;
    }

    public void Execute()
    {
        foreach (var entity in _mobs.GetEntities())
        {
            entity.ReplacePosition(entity.mobUnit.value.transform.position);
            entity.ReplaceDirection(entity.mobUnit.value.transform.rotation.z);
            var rg2d = entity.mobUnit.value.GetComponent<Rigidbody2D>();
            if (rg2d != null)
            {
                entity.ReplaceSpeed(rg2d.velocity.magnitude);
            }
        }
    }
}