using Entitas;
using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

public class ProduceUidSystem : ReactiveSystem<GameEntity>
{
    public ProduceUidSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override void Execute(System.Collections.Generic.List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var uid = UidGenerator.nextId;
            entity.uidUnit.value.uid = uid;
            entity.AddFrogUid(uid);
        }
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.MobUnit, GameMatcher.UidUnit));
    }
}