//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputContext {

    public InputEntity spawnFactoryIdEntity { get { return GetGroup(InputMatcher.SpawnFactoryId).GetSingleEntity(); } }
    public SpawnFactoryIdComponent spawnFactoryId { get { return spawnFactoryIdEntity.spawnFactoryId; } }
    public bool hasSpawnFactoryId { get { return spawnFactoryIdEntity != null; } }

    public InputEntity SetSpawnFactoryId(string newValue) {
        if (hasSpawnFactoryId) {
            throw new Entitas.EntitasException("Could not set SpawnFactoryId!\n" + this + " already has an entity with SpawnFactoryIdComponent!",
                "You should check if the context already has a spawnFactoryIdEntity before setting it or use context.ReplaceSpawnFactoryId().");
        }
        var entity = CreateEntity();
        entity.AddSpawnFactoryId(newValue);
        return entity;
    }

    public void ReplaceSpawnFactoryId(string newValue) {
        var entity = spawnFactoryIdEntity;
        if (entity == null) {
            entity = SetSpawnFactoryId(newValue);
        } else {
            entity.ReplaceSpawnFactoryId(newValue);
        }
    }

    public void RemoveSpawnFactoryId() {
        spawnFactoryIdEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity {

    public SpawnFactoryIdComponent spawnFactoryId { get { return (SpawnFactoryIdComponent)GetComponent(InputComponentsLookup.SpawnFactoryId); } }
    public bool hasSpawnFactoryId { get { return HasComponent(InputComponentsLookup.SpawnFactoryId); } }

    public void AddSpawnFactoryId(string newValue) {
        var index = InputComponentsLookup.SpawnFactoryId;
        var component = CreateComponent<SpawnFactoryIdComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceSpawnFactoryId(string newValue) {
        var index = InputComponentsLookup.SpawnFactoryId;
        var component = CreateComponent<SpawnFactoryIdComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveSpawnFactoryId() {
        RemoveComponent(InputComponentsLookup.SpawnFactoryId);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class InputMatcher {

    static Entitas.IMatcher<InputEntity> _matcherSpawnFactoryId;

    public static Entitas.IMatcher<InputEntity> SpawnFactoryId {
        get {
            if (_matcherSpawnFactoryId == null) {
                var matcher = (Entitas.Matcher<InputEntity>)Entitas.Matcher<InputEntity>.AllOf(InputComponentsLookup.SpawnFactoryId);
                matcher.componentNames = InputComponentsLookup.componentNames;
                _matcherSpawnFactoryId = matcher;
            }

            return _matcherSpawnFactoryId;
        }
    }
}
