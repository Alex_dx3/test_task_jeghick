//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity {

    static readonly UnitEnterCupComponent unitEnterCupComponent = new UnitEnterCupComponent();

    public bool isUnitEnterCup {
        get { return HasComponent(InputComponentsLookup.UnitEnterCup); }
        set {
            if (value != isUnitEnterCup) {
                var index = InputComponentsLookup.UnitEnterCup;
                if (value) {
                    var componentPool = GetComponentPool(index);
                    var component = componentPool.Count > 0
                            ? componentPool.Pop()
                            : unitEnterCupComponent;

                    AddComponent(index, component);
                } else {
                    RemoveComponent(index);
                }
            }
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class InputMatcher {

    static Entitas.IMatcher<InputEntity> _matcherUnitEnterCup;

    public static Entitas.IMatcher<InputEntity> UnitEnterCup {
        get {
            if (_matcherUnitEnterCup == null) {
                var matcher = (Entitas.Matcher<InputEntity>)Entitas.Matcher<InputEntity>.AllOf(InputComponentsLookup.UnitEnterCup);
                matcher.componentNames = InputComponentsLookup.componentNames;
                _matcherUnitEnterCup = matcher;
            }

            return _matcherUnitEnterCup;
        }
    }
}
