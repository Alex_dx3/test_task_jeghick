//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public UidUnitComponent uidUnit { get { return (UidUnitComponent)GetComponent(GameComponentsLookup.UidUnit); } }
    public bool hasUidUnit { get { return HasComponent(GameComponentsLookup.UidUnit); } }

    public void AddUidUnit(Scripts.Uid.UidUnit newValue) {
        var index = GameComponentsLookup.UidUnit;
        var component = CreateComponent<UidUnitComponent>(index);
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceUidUnit(Scripts.Uid.UidUnit newValue) {
        var index = GameComponentsLookup.UidUnit;
        var component = CreateComponent<UidUnitComponent>(index);
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveUidUnit() {
        RemoveComponent(GameComponentsLookup.UidUnit);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherUidUnit;

    public static Entitas.IMatcher<GameEntity> UidUnit {
        get {
            if (_matcherUidUnit == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.UidUnit);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherUidUnit = matcher;
            }

            return _matcherUidUnit;
        }
    }
}
