using Entitas;
using Entitas.CodeGeneration.Attributes;
using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

[Game]
public class FrogComponent : IComponent
{

}

[Game]
public class CupComponent : IComponent
{ }

[Game]
public class PositionComponent : IComponent
{
    public Vector2 value;
}

[Game]
public class DirectionComponent : IComponent
{
    public float value;
}

[Game]
public class SpeedComponent : IComponent
{
    public float value;
}

[Game]
public class InsideCupComponent : IComponent
{

}
[Game]
public class MobUnitComponent : IComponent
{
    public BaseUnit value;
}

[Game]
public class UidUnitComponent : IComponent
{
    public UidUnit value;
}

[Game]
public class FrogUidComponent : IComponent
{
    [PrimaryEntityIndex]
    public int value;
}

