using Entitas;
using Entitas.CodeGeneration.Attributes;
using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

[Input, Unique]
public class ClickComponent : IComponent
{
    public Vector2 value;
}

[Input]
public class TargetUnitComponent : IComponent
{

    public BaseUnit value;
}
[Input]
public class TargetUidComponent : IComponent
{
    public UidUnit value;
}
[Input, Unique]
public class SpawnFactoryIdComponent : IComponent
{
    public string value;
}

[Input]
public class UnitEnterCupComponent : IComponent
{

}
[Input]
public class UnitExitCupComponent : IComponent
{

}
[Input]
public class EventFlagComponent : IComponent
{

}


