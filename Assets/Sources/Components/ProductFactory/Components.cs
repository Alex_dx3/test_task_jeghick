using Entitas;
using Entitas.CodeGeneration.Attributes;
using ProductFactory;

[ProductFactory]
public class FactoryUnitComponent : IComponent
{
    public FactoryUnit value;
}

[ProductFactory]
public class FactoryIdComponent : IComponent
{
    [PrimaryEntityIndex]
    public string value;
}

