﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProductFactory
{
    public sealed class ProductUnit : MonoBehaviour
    {
        public delegate void FactoryDespawn(ProductUnit unit);
        public FactoryDespawn factoryDespawn;

        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }

        public void Despawn()
        {
            if (factoryDespawn != null) factoryDespawn(this);
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
