﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProductFactory
{
    public class FactoryUnit : MonoBehaviour
    {
        public string factoryId;
        public GameObject prefab;
        public int preloadCount;

        private Queue<ProductUnit> __productQueue = new Queue<ProductUnit>();

        private int __currentId;

        private ProductFactoryEntity __entity;

        private void Start()
        {

            if (prefab == null)
            {
                Debug.LogError("Factory does not have prefab. | " + gameObject.name, gameObject);
                gameObject.SetActive(false);
                return;
            }

            if (prefab.GetComponent<ProductUnit>() != null)
            {
                Debug.LogError("Factory prefab already has ProductUnit. This is unexpected. | " + gameObject.name, gameObject);
                gameObject.SetActive(false);
                return;
            }

            if (string.IsNullOrEmpty(factoryId))
            {
                Debug.LogError("Factory does not have ID. | " + gameObject.name, gameObject);
                gameObject.SetActive(false);
                return;
            }

            if (Contexts.sharedInstance.productFactory.GetEntityWithFactoryId(factoryId) == null)
            {
                __entity = Contexts.sharedInstance.productFactory.CreateEntity();
                __entity.AddFactoryUnit(this);
                __entity.AddFactoryId(factoryId);
            }
            else
            {
                Debug.LogError("Factory with ID " + factoryId + " already registerd. Can not activate. | " + gameObject.name, gameObject);
                gameObject.SetActive(false);
            }

            for (int i = 0; i < preloadCount; i++)
            {
                _Preload();
            }
        }

        protected virtual void _Preload()
        {
            __currentId++;

            var go = Instantiate(prefab, transform, true);
            go.name = __currentId.ToString();

            var product = go.AddComponent<ProductUnit>();
            product.factoryDespawn = Despawn;
            product.transform.localPosition = Vector3.zero;
            product.SetActive(false);

            __productQueue.Enqueue(product);
        }
        public virtual ProductUnit Spawn()
        {
            if (__productQueue.Count == 0) _Preload();

            var product = __productQueue.Dequeue();
            product.SetActive(true);

            return product;
        }

        public virtual void Despawn(ProductUnit product)
        {
            product.SetActive(false);
            __productQueue.Enqueue(product);
        }
    }
}
