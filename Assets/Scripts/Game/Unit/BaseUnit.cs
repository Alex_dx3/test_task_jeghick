using UnityEngine;
using DG.Tweening;

namespace Scripts.Game.Unit
{
    public abstract class BaseUnit : MonoBehaviour
    {
        public void Start()
        {
            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.AddMobUnit(this);
            this.OnSpawn();
        }
        public virtual void OnSpawn() { }
        public virtual void OnTap(Vector2 tap) { }

        public virtual void OnStayStill() { }

        public abstract bool Spawner { get; }
    }
}