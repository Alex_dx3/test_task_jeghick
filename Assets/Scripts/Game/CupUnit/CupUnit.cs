using Scripts.Game.Unit;
using Scripts.Uid;
using UnityEngine;

namespace Scripts.Game.CupUnit
{
    public class CupUnit : BaseUnit
    {
        public override bool Spawner { get { return false; } }

        void OnTriggerEnter2D(Collider2D col)
        {
            Debug.Log("Enter: " + col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
            var enterRequest = Contexts.sharedInstance.input.CreateEntity();
            enterRequest.isEventFlag = true;
            enterRequest.isUnitEnterCup = true;
            enterRequest.AddTargetUnit(col.GetComponent<BaseUnit>());
            enterRequest.AddTargetUid(col.GetComponent<UidUnit>());
        }
        void OnTriggerExit2D(Collider2D col)
        {
            Debug.Log("Exit: " + col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
            var exitRequest = Contexts.sharedInstance.input.CreateEntity();
            exitRequest.isEventFlag = true;
            exitRequest.isUnitExitCup = true;
            exitRequest.AddTargetUnit(col.GetComponent<BaseUnit>());
            exitRequest.AddTargetUid(col.GetComponent<UidUnit>());
        }
    }
}