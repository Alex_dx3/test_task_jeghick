using UnityEngine;

namespace Scripts.Game.FrogUnit
{
    [CreateAssetMenu(fileName = "FrogData", menuName = "Data/Frog", order = 1)]
    public class FrogUnitConfig : ScriptableObject
    {
        public float MaxSize = 1;
        public float MinSize = .1f;
        public PhysicsMaterial2D Material = null;
        public float MaxForceOnTap = 500;
        public float MinForceOnTap = 100;
        public float MaxRotationForceOnTap = 300;
        public float MinRotationForceOnTap = 100; 
    }
}