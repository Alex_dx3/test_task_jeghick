using Scripts.Game.Unit;
using DG.Tweening;
using UnityEngine;
using Scripts.Uid;

namespace Scripts.Game.FrogUnit
{
    public class FrogUnit : BaseUnit
    {
        private Rigidbody2D _rg2d;
        protected Rigidbody2D rg2d
        {
            get
            {
                return _rg2d ?? (_rg2d = GetComponent<Rigidbody2D>());
            }
        }
        public FrogUnitConfig config;
        public FrogUnit() : base()
        {

        }
        public void Start()
        {
            var entity = Contexts.sharedInstance.game.CreateEntity();
            entity.AddMobUnit(this);
            entity.AddUidUnit(GetComponent<UidUnit>());
            var randomScale = Random.Range(config.MinSize, config.MaxSize);
            transform.localScale = new Vector2(randomScale, randomScale);
            GetComponent<Rigidbody2D>().sharedMaterial = config.Material;
            this.OnSpawn();
        }
        public override bool Spawner { get { return false; } }
        public override void OnSpawn()
        {
            transform.DOPunchScale(new Vector2(0.1f, 0.1f), 1, 5);
        }
        public override void OnTap(Vector2 tap)
        {
            Debug.Log("on tap");
            var force = Vector2.up * Random.Range(config.MinForceOnTap, config.MaxForceOnTap);
            var rotation = Random.Range(config.MinRotationForceOnTap, config.MaxRotationForceOnTap);
            this.rg2d.AddTorque(rotation);
            this.rg2d.AddForceAtPosition(force, tap);
        }

        public override void OnStayStill()
        {
            this.transform.DORotate(new Vector3(0, 0, 0), 1, RotateMode.Fast);
        }
    }
}