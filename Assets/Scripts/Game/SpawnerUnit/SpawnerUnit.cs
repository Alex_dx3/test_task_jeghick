using Scripts.Game.Unit;

namespace Scripts.Game.SpawnerUnit
{
    public class SpawnerUnit : BaseUnit
    {
        public override bool Spawner { get { return true; } }
    }
}