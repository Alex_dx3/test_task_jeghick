﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Uid
{
    public class UidUnit : MonoBehaviour
    {
        public int uid = -1;

        public override int GetHashCode()
        {
            return uid;
        }
    }
}
