﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Uid
{
    public static class UidGenerator
    {
        static int _nextId = 0;
        public static int nextId
        {
            get
            {
                if (freeUids.Count > 0)
                {
                    return freeUids.Dequeue();
                }

                _nextId++;
                return _nextId;
            }
        }

        static Queue<int> freeUids = new Queue<int>();
        public static void FreeUid(int uid)
        {
            freeUids.Enqueue(uid);
        }
    }
}
